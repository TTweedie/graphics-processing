
import processing.video.*;

Movie background;
Movie monkey;

void setup() {
   size(1280, 960);
   background = new Movie(this, "Quadrangle.mov");
   monkey = new Movie(this, "monkey.avi");
   monkey.loop();
   background.loop();

}

void draw(){
  PImage monkeyMatte;
  PImage mattedMonkey;
  PImage mattedBackground;
  //
  monkeyMatte=createMatte(monkey);
  mattedMonkey=monkey.copy();
  mattedMonkey.mask(monkeyMatte);
  //
  //
  PImage smallMatte = monkeyMatte.copy();
  smallMatte.resize(background.width, background.height);
  mattedBackground=background.copy();
  mattedBackground.mask(invert(smallMatte));
     //
  background(0);
  PImage smallMonkey = mattedMonkey.copy();
  smallMonkey.resize(mattedBackground.width, mattedBackground.height);
  smallMonkey.blend(mattedBackground, 0, 0 ,smallMonkey.width, smallMonkey.height, 0, 0 ,smallMonkey.width, smallMonkey.height, ADD);
  smallMonkey.resize(smallMonkey.width*3, smallMonkey.height*3);
  image(smallMonkey,0,0);
}

void movieEvent(Movie m) {
  if(m == background){
     background.read(); //<>//
  }
   if(m==monkey){
     monkey.read();
   }
}

PImage createMatte(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   if(isPixelRemoved(color(img.pixels[i]), 0)){
     returnimg.pixels[i]=color(0);
   }
   else{
     returnimg.pixels[i]=color(255);
   }
 }
 return returnimg;
}

boolean isPixelRemoved(color c, int colour){

  if(colour==0){
   //println(blue(c), red(c), green(c));
   if(blue(c)>140){
    return true; 
   }
   return false;
  }
  
  
  return false;
}

PImage invert(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   if(color(img.pixels[i])==color(255)){
     returnimg.pixels[i]=color(0);
   }
   else{
     returnimg.pixels[i]=color(255);
   }
 }
 return returnimg;
}
