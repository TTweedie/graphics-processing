TrackingPoints kMClustering(ArrayList<Vector2D> redPoints, int frameheight, int framewidth, int noOfIterations){
  Vector2D[] averages = new Vector2D[5];
  for(int i=0;i<5;i++){
    averages[i]=new Vector2D((int)random(framewidth), (int)random(frameheight));
  }
  for(int i=0;i<noOfIterations;i++){
    ArrayList<ArrayList<Vector2D>> groups = new ArrayList<ArrayList<Vector2D>>();
    for(int j=0;j<5;j++){
      groups.add(new ArrayList<Vector2D>());
    }
    for(Vector2D point: redPoints){
      int group=0;
      float distance=999999;
      for(int j=0;j<5;j++){
        if(point.distance(averages[j])<distance&&(groups.get(j).size()<redPoints.size()/5)){//Checks if the group already has a lot of points in it
          distance=point.distance(averages[j]);
          group=j;
        }
      }
      groups.get(group).add(point);
    }
    for(int j=0;j<5;j++){
      if(groups.get(j).size()!=0){
        averages[j]=getMedian(groups.get(j));
      }
    }
  }
  TrackingPoints ret = new TrackingPoints();
  for(int i=0;i<5;i++){
    ret.addUnsortedPoint(averages[i]);
  }
  return ret;
}


TrackingPoints nearestNeighbourClustering(ArrayList<Vector2D> redPoints, int frameheight, int framewidth, int noOfIterations){
  Vector2D spread=getSpread(redPoints);
  println(spread.toPrintable());
  Vector2D bodyNN=getAverage(redPoints);
  Vector2D LeftArmNN=new Vector2D((int)(bodyNN.x-0.375*spread.x), (int)(bodyNN.y-0.375*spread.y));
  Vector2D RightArmNN=new Vector2D((int)(bodyNN.x+spread.x*0.375), (int)(bodyNN.y-0.375*spread.y));
  Vector2D LeftLegNN=new Vector2D((int)(bodyNN.x-0.25*spread.x), (int)(bodyNN.y+0.375*spread.y));
  Vector2D RightLegNN=new Vector2D((int)(bodyNN.x+spread.x*0.25), (int)(bodyNN.y+0.375*spread.y));
  
  Vector2D[] NNPoints = new Vector2D[5];
  NNPoints[0]=bodyNN;
  NNPoints[1]=LeftArmNN;
  NNPoints[2]=RightArmNN;
  NNPoints[3]=LeftLegNN;
  NNPoints[4]=RightLegNN;
  ArrayList<ArrayList<Vector2D>> groups = new ArrayList<ArrayList<Vector2D>>();
  for(int i=0;i<5;i++){
    groups.add(new ArrayList<Vector2D>());
  }
  for(Vector2D point: redPoints){
    float minDistance=100000;
    int group=0;
    for(int i=0;i<5;i++){
      if(point.distance(NNPoints[i])<minDistance){
        minDistance=point.distance(NNPoints[i]);
        group=i;
      }
    }
    groups.get(group).add(point);
  }
  TrackingPoints ret= new TrackingPoints();
  for(int i=0;i<5;i++){
    ret.addUnsortedPoint(new Vector2D());
  }
  
  /*
  ret.setBody(NNPoints[0]);
  ret.setLeftArm(NNPoints[1]);
  ret.setRightArm(NNPoints[2]);
  ret.setLeftLeg(NNPoints[3]);
  ret.setRightLeg(NNPoints[4]);
  */
  
  ret.setBody(getAverage(groups.get(0)));
  ret.setLeftArm(getAverage(groups.get(1)));
  ret.setRightArm(getAverage(groups.get(2)));
  ret.setLeftLeg(getAverage(groups.get(3)));
  ret.setRightLeg(getAverage(groups.get(4)));
  return ret;
}


Vector2D getAverage(ArrayList<Vector2D> list){
  if(list.size()==0){
     return new Vector2D(); 
  }
  Vector2D average = new Vector2D();
  for(Vector2D vec: list){
    average.add(vec);
  }
  average.divide(list.size());
  return average;
}

Vector2D getMedian(ArrayList<Vector2D> list){
  ArrayList<Vector2D> sorted = vecSort(list);
  return sorted.get(list.size()/2);
}

Vector2D getSpread(ArrayList<Vector2D> list){
  Vector2D max=new Vector2D();
  Vector2D min=new Vector2D(10000,10000);
  for(Vector2D vec :list){
    if(vec.x>max.x){
      max.x=vec.x;
    }
    if(vec.y>max.y){
      max.y=vec.y;
    }
    if(vec.x<min.x){
      min.x=vec.x;
    }
    if(vec.y<min.y){
      min.y=vec.y;
    }
  }
  return new Vector2D(max.x-min.x, max.y-min.y);
}
