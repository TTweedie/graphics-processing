ArrayList<Vector2D> insertionSort(ArrayList<Vector2D> list){
  ArrayList<Vector2D> sorted= new ArrayList<Vector2D>();
  for(int i=0;i<list.size();i++){
    Vector2D max=new Vector2D();
    for(int j=0;j<list.size();j++){
      if(list.get(j).greaterThan(max)){
        max=list.get(j); 
      }
    }
    list.remove(max);
    sorted.add(max);
  }
  return sorted;
}

ArrayList<Vector2D> vecSort(ArrayList<Vector2D> list){
 return insertionSort(list); 
}
