class Arrow{
  ArrayList<Boid> arrows;
  PImage texture;
  SoundFile whoosh;
  Boids toKill;
  
  Arrow(SoundFile woos, Boids toKil){
    arrows=new ArrayList<Boid>();
    whoosh = woos;
    texture=loadImage("Arrow.png");
    toKill=toKil;
  }
  
  void createArrow(float x, float y){
    arrows.add(new Boid(new PVector(30,0), new PVector(x,y),50,10, texture));
    whoosh.play();
  }
  
  void deleteArrow(Boid b){
    if(b.position.x>600){
     arrows.remove(b); 
    }
  }
  
  void checkHit(){
   for(Boid boid:toKill.boids){
     for(Boid a:arrows){
       if(a.position.dist(boid.position)<30){
         boid.dead=true;
       }
     }
   }
  }
  void render(){
    for(int i=0;i<arrows.size();i++){
      arrows.get(i).position.add(arrows.get(i).velocity);
      arrows.get(i).render();
      checkHit();
      deleteArrow(arrows.get(i));
    }
  }
}
