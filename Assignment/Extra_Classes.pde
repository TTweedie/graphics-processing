class Vector2D{
  int x;
  int y;
  
  Vector2D(){
   x=0;
   y=0;
  }
  Vector2D(int ix, int iy){
   x=ix;
   y=iy;
  }
  Vector2D(PVector p){
   x=(int)p.x;
   y=(int)p.y;
  }
  Vector2D(Vector2D p){
   x=p.x;
   y=p.y;
  }
  
  
  PVector getp(){
   return new PVector(x,y);
  }
  
  void add(Vector2D rhs){
    x+=rhs.x;
    y+=rhs.y;
  }
  void sub(Vector2D rhs){
    x-=rhs.x;
    y-=rhs.y;
  }
  void divide(Vector2D rhs){
    x/=rhs.x;
    y/=rhs.y;
  }
  void divide(int rhs){
    x/=rhs;
    y/=rhs;
  }
  boolean lessThan(Vector2D rhs){
    if(x+y<rhs.x+rhs.y){
     return true; 
    }
    return false;
  }
  boolean greaterThan(Vector2D rhs){
    if(x+y>rhs.x+rhs.y){
     return true; 
    }
     return false;
  }
  float distance(Vector2D p){
    return sqrt(pow(x-p.x,2)+pow(y-p.y,2));
  }
  
  String toPrintable(){
    return "|X:"+x+" Y:"+y+"|";
  }
}






class TrackingPoints{
  private ArrayList<Vector2D> m_points;
  private boolean m_sorted;
  
  TrackingPoints(){
   m_sorted=false;
   m_points=new ArrayList<Vector2D>();
  }
  
  void addUnsortedPoint(Vector2D point){
    m_points.add(point);
    m_sorted=true;
  }
  
  Vector2D getBody(){
    return new Vector2D(m_points.get(0));
  }
  Vector2D getLeftArm(){
    return new Vector2D(m_points.get(1));
  }
  Vector2D getRightArm(){
    return new Vector2D(m_points.get(2));
  }
  Vector2D getLeftLeg(){
    return new Vector2D(m_points.get(3));
  }
  Vector2D getRightLeg(){
    return new Vector2D(m_points.get(4));
  }
  
  void setBody(Vector2D vec){
    m_points.set(0, vec);
  }
  void setLeftArm(Vector2D vec){
    m_points.set(1, vec);
  }
  void setRightArm(Vector2D vec){
    m_points.set(2, vec);
  }
  void setLeftLeg(Vector2D vec){
    m_points.set(3, vec);
  }
  void setRightLeg(Vector2D vec){
    m_points.set(4, vec);
  }
  
  void sortPoints(){
    Vector2D Body=new Vector2D();
    Vector2D LeftLeg=new Vector2D();
    Vector2D RightLeg=new Vector2D();
    Vector2D LeftArm=new Vector2D();
    Vector2D RightArm=new Vector2D();
    for(Vector2D pointi : m_points){
      int left=0;
      int right=0;
      int above=0;
      int below=0;
      for(Vector2D pointj : m_points){
        if(pointi.x<pointj.x){
          left++;
        }
        else if(pointi.x>pointj.x){
          right++;
        }
        if(pointi.y<pointj.y){
          above++;
        }
        else if(pointi.y>pointj.y){
          below++;
        }
      }
      if(left>2){
       if(above>=2){
         LeftArm=pointi;
       }
       else{
         LeftLeg=pointi;
       }
      }
      else if(right>2){
       if(above>=2){
         RightArm=pointi;
       }
       else{
         RightLeg=pointi;
       }
      }
      else{
         Body=pointi; 
      }
    }
    m_points.set(0,Body);
    m_points.set(1,LeftArm);
    m_points.set(2,RightArm);
    m_points.set(3,LeftLeg);
    m_points.set(4,RightLeg);
  }
  
  void render(){
    fill(255,0,0);
    ellipse(getBody().x, getBody().y, 10, 10);
    fill(0,255,0);
    ellipse(getLeftArm().x, getLeftArm().y, 10, 10);
    fill(0,255,0);
    ellipse(getRightArm().x, getRightArm().y, 10, 10);
    fill(0,0,255);
    ellipse(getLeftLeg().x, getLeftLeg().y, 10, 10);
    fill(0,0,255);
    ellipse(getRightLeg().x, getRightLeg().y, 10, 10);
  }
  String toPrintable(){
    return "Body "+getBody().toPrintable()+" Left Arm "+getLeftArm().toPrintable()+" Right Arm "+getRightArm().toPrintable()+" Left Leg "+getLeftLeg().toPrintable()+" Right Leg "+getRightLeg().toPrintable();
  }
}
