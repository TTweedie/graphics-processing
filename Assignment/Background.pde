class Background{
 PImage trainWindow;
 PImage sky;
 ArrayList<PImage> texturePool;
 ArrayList<BackgroundObject> objects;
 int floor = 300;
 int horizon = 170;
 
 Background(PImage frame){
   texturePool=new ArrayList<PImage>();
   objects=new ArrayList<BackgroundObject>();
   trainWindow=loadImage("TrainWindow.png");
   trainWindow.resize(frame.width, frame.height);
   sky=loadImage("background.png");
   sky.resize(frame.width, frame.height);
   texturePool.add(loadImage("giraffe.png"));
   texturePool.add(loadImage("rhino.png"));
   texturePool.add(loadImage("tree.png"));
   texturePool.add(loadImage("bush.png"));
   for(int i=0;i<10;i++){
     objects.add(new BackgroundObject((int)random(20), (int)random(300,500),floor,horizon, texturePool.get((int)random(texturePool.size())))); 
   }
 }
 
 void addNewObjects(){
   for(int i=0;i<objects.size();i++){
    if(objects.get(i).position.x<0){
     objects.remove(i);
     objects.add(new BackgroundObject((int)random(20), (int)random(500,700),floor,horizon, texturePool.get((int)random(texturePool.size())))); 
    }
   }
 }
 void render(){
  addNewObjects();
  image(sky, 0,0); 
  for(BackgroundObject b : objects){
   b.render(); 
  }
  image(trainWindow,0,0);
 }
}

class BackgroundObject{
  int distance;
  Vector2D position;
  PShape shape;
  
  BackgroundObject(int dist,int x, int floor, int horizon, PImage texture){
    distance=dist;
    float distancerat=(float)1/(float)distance;
    position=new Vector2D(x, (int)(horizon+((floor-horizon)*distancerat)));
    float blength=((distancerat)*400);
    float bwidth=(((float)blength/texture.height)*texture.width);
    shape=createShape(RECT, 0,0,bwidth, blength);
    shape.setTexture(texture);
  }
  
  void render(){
     int changeinX=(int)-((1.0/distance)*20);
     position.add(new Vector2D(changeinX, 0));
     shape.resetMatrix();
     shape.translate(position.x, position.y);
     shape(shape);
  }
  
}
