import processing.video.*;
import java.util.Collections;
import processing.sound.*;


boolean[][] se={{false, true, false}, {true, true, true}, {false, true, false}};


Movie toTrack;
TrackingPoints point;
Background bg;
Boids intob1;
Arrow intob2;
SoundFile buzz;
SoundFile whoosh;
Character c;
boolean setup = false;

void setup(){
    size (568,320);
    buzz = new SoundFile(this, "Fly.wav");
    whoosh = new SoundFile(this, "Arrow.wav");
    toTrack = new Movie(this, "monkey.mov");
    toTrack.loop();
    toTrack.speed(0.75);
}

void draw(){
  background(125);
  //image(toTrack,0,0);
  //image(showTrackedColours(toTrack),0,toTrack.height);
  //image(erosion(showTrackedColours(toTrack),se,1,1),toTrack.width,toTrack.height);
  if(setup){
    point = nearestNeighbourClustering(getRedPointsEroded(toTrack),toTrack.height, toTrack.width, 10);
    bg.render();
    c.updatePartPositions(point);
    c.render();
    ArrayList<PVector> avoid = new ArrayList<PVector>();
    avoid.add(new PVector(point.getLeftArm().x, point.getLeftArm().y));
    avoid.add(new PVector(point.getRightArm().x, point.getRightArm().y));
    ArrayList<PVector> attract = new ArrayList<PVector>();
    attract.add(new PVector(point.getBody().x, point.getBody().y-100));
    intob1.render(avoid,attract);
    intob2.render();
    //point.render();
  }
}

void mouseReleased(){
 if(mouseButton == LEFT){
   intob2.createArrow(mouseX, mouseY);
 }
 if(mouseButton == RIGHT){
      buzz.stop();
      whoosh.stop();
 }  
}
void movieEvent(Movie m){
  m.read();
  if(!setup){
    point = nearestNeighbourClustering(getRedPointsEroded(toTrack),toTrack.height, toTrack.width, 10);
    c=new Character();
    c.addBodyParts(point);
    bg = new Background(m);
    intob1=new Boids(buzz); //<>//
    intob2= new Arrow(whoosh, intob1);
  }
  setup=true;
}

Vector2D getXYFromIndex(int index, int width){
  //index=y*width+x
  Vector2D retval= new Vector2D();
  retval.x=index%width;
  retval.y=(index-retval.x)/width;
  return retval;
}

int getIndexFromXY(int x, int y, int width){
  return y*width+x;
}

boolean isTrackedColour(color c){
  //println(blue(c), red(c), green(c));
  if(red(c)>150&&blue(c)<150&&green(c)<150){
    //println("red "+red(c)+"blue "+blue(c)+"green "+green(c));
    return true; 
  }
  if(red(c)>240&&blue(c)<100&&green(c)<100){
    //println("red "+red(c)+"blue "+blue(c)+"green "+green(c));
    return true; 
  }
 return false;
}

ArrayList<Vector2D> getRedPoints(PImage frame){ 
 int endloc=frame.height*frame.width;
 ArrayList<Vector2D> redPoints = new ArrayList<Vector2D>();
 for (int i=0;i<endloc;i++){
   if(isTrackedColour(color(frame.pixels[i]))){
     redPoints.add(getXYFromIndex(i, frame.width));
   }
 }
 return redPoints;
}

ArrayList<Vector2D> getRedPointsEroded(PImage frame){ 
 int endloc=frame.height*frame.width;
 ArrayList<Vector2D> redPoints = new ArrayList<Vector2D>();
 PImage eroded=erosion(showTrackedColours(frame),se,1,1);
 for (int i=0;i<endloc;i++){
   if(color(eroded.pixels[i])==color(255)){
     redPoints.add(getXYFromIndex(i, frame.width));
   }
 }
 return redPoints;
}

PImage showTrackedColours(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   if(isTrackedColour(img.pixels[i])){
     returnimg.pixels[i]=color(255);
   }
   else{
     returnimg.pixels[i]=color(0);
   }
 }
 return returnimg;
}

PImage erosion(PImage img, boolean[][]se, int originx, int originy){
  PImage returnimg = createImage(img.width, img.height, ALPHA);
  int endloc=img.height*img.width;
  for(int imgloc=0;imgloc<endloc;imgloc++){
    boolean fillpixel=true;
    for(int i=0;i<se.length;i++){
      for(int j=0;j<se[i].length;j++){
        int offset=(j-originx)+(img.width*(i-originy));
        int serelativeloc=constrain(imgloc+offset, 0, endloc-1);
        if((!(color(img.pixels[serelativeloc])==color(255)))&&(se[i][j])){
          fillpixel=false;
        }
    }
  }
  if(fillpixel){
    returnimg.pixels[imgloc]=color(255);
  }
  else{
   returnimg.pixels[imgloc]=color(0); 
  }
}
  return returnimg;
}
