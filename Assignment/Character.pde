class Character{
  
  ArrayList<BodyPart> parts;
  
  int shoulderOffset=10;
  int waistOffset=20;
  int headOffset=100;
  
  Character(){
   parts=new ArrayList<BodyPart>(); 
  }
  
  void addBodyParts(TrackingPoints points){
    parts=new ArrayList<BodyPart>(); 
    BodyPart body= new BodyPart(points.getBody(),loadImage("Body.png"));
    BodyPart leftLeg= new BodyPart(points.getBody(),points.getLeftLeg(), new Vector2D(-20,10),loadImage("LeftLeg.png"));
    BodyPart rightLeg= new BodyPart(points.getBody(),points.getRightLeg(), new Vector2D(20,10),loadImage("RightLeg.png"));
    BodyPart leftArm= new BodyPart(points.getBody(),points.getLeftArm(), new Vector2D(-20,-50),loadImage("LeftArm.png"));
    BodyPart rightArm= new BodyPart(points.getBody(),points.getRightArm(), new Vector2D(20,-50),loadImage("RightArm.png"));
    parts.add(body);
    parts.add(leftLeg);
    parts.add(rightLeg);
    parts.add(leftArm);
    parts.add(rightArm);
  }
  
  void updatePartPositions(TrackingPoints points){
    parts.get(0).updatePositionAndRotation(points.getBody(), points.getBody());
    parts.get(1).updatePositionAndRotation(points.getBody(), points.getLeftLeg());
    parts.get(2).updatePositionAndRotation(points.getBody(),points.getRightLeg());
    parts.get(3).updatePositionAndRotation(points.getBody(),points.getLeftArm());
    parts.get(4).updatePositionAndRotation(points.getBody(),points.getRightArm());
  }
  
  void render(){
    for(BodyPart p:parts){
      p.render(); 
    }
  }
}

class BodyPart{
 PShape part;
 Vector2D relativePivot;
 Vector2D position;
 float rotation;
 
 BodyPart(Vector2D pivotPoint, Vector2D endPoint, Vector2D relative, PImage texture){
   relativePivot=relative;
   pivotPoint.add(relativePivot);
   float partLength=100;//pivotPoint.distance(endPoint);
   float partWidth=((float)partLength/texture.height) * texture.width;
   position=pivotPoint;
   part=createShape(RECT,-partWidth/2,0, partWidth, partLength);
   part.setTexture(texture);
   rotation=atan2(pivotPoint.x-endPoint.x, endPoint.y-pivotPoint.y);

 }
 
  BodyPart(Vector2D bodyPoint, PImage texture){
   relativePivot=new Vector2D();
   float partLength=150;
   float partWidth=((float)partLength/texture.height) * texture.width;
   position=bodyPoint;
   part=createShape(RECT,-partWidth/2, -partLength+partLength/8, partWidth, partLength);
   part.setTexture(texture);
   rotation=0;
 }
 
 void updatePositionAndRotation(Vector2D pivotPoint, Vector2D endPoint){
   pivotPoint.add(relativePivot);
   position=pivotPoint;
   rotation=atan2(pivotPoint.x-endPoint.x, endPoint.y-pivotPoint.y);
 }
 
 void render(){
    part.resetMatrix();
    part.translate(position.x, position.y);
    part.rotate(rotation);
    shape(part);
    //fill(255,0,0);
    //ellipse(position.x,position.y ,20,20);
 }
}
