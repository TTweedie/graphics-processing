//Boid algorithm adapted from http://www.kfish.org/boids/pseudocode.html

int DISTANCEBUFFER = 50;

class Boid{
 PVector velocity;
 PVector position;
 PShape shape;
 boolean dead;
 int deadtime=0;
 
 Boid(PVector v, PVector p, PImage texture){
   velocity=v;
   position=p;
   shape = createShape(RECT, 0,0,20,20);
   shape.setTexture(texture);
 }
 
  Boid(PVector v, PVector p, int bwidth, int bheight, PImage texture){
   velocity=v;
   position=p;
   shape = createShape(RECT, 0,0,bwidth,bheight);
   shape.setTexture(texture);
 }
 
 void render(){
    shape.resetMatrix();
    shape.translate(position.x, position.y);
    shape(shape);
 }
}

class Boids{
 ArrayList<Boid> boids;
 ArrayList<PVector> m_avoidPoints;
 ArrayList<PVector> m_attractPoints;
 SoundFile buzz;
 int wallbot;
 int wallleft=0;
 int walltop=0;
 int wallright;
 
 Boids(SoundFile buz){
  wallbot=320;
  wallright=568;
  boids=new ArrayList<Boid>();
  PImage texture=loadImage("Fly.png");
  buzz=buz;
  for(int i=0;i<50;i++){
    boids.add(new Boid(new PVector((int)random(2),(int)random(1)), new PVector((int)random(568),(int)random(320)), texture));
  }
 }
 
 void render(ArrayList<PVector> avoidPoints, ArrayList<PVector> attractPoints){
  for(Boid boid:boids){
   m_avoidPoints=avoidPoints;
   m_attractPoints=attractPoints;
   moveAllBoids();
   boid.render(); 
  }
 }
 
 void moveAllBoids(){
   for(Boid boid:boids){ //<>//
     if(boid.dead){
       if(boid.position.y<300){
         boid.velocity=new PVector(0,1);
         boid.position.add(boid.velocity);
       }
       else{
         if(boid.deadtime<11000){
           boid.deadtime++;
         }
         else{
           boid.dead=false;
           boid.deadtime=0;
         }
       }
     }
     else{
       PVector v1=flock(boid);
       PVector v2=avoid(boid);
       PVector v3=matchVelocity(boid);
       PVector v4=avoidWalls(boid);
       PVector v5=avoidPoints(boid);
       PVector v6=goToPoints(boid);
       boid.velocity.add(v1);
       boid.velocity.add(v2);
       boid.velocity.add(v3);
       boid.velocity.add(v4);
       boid.velocity.add(v5);
       boid.velocity.add(v6);
       PVector slowVelocity = boid.velocity;
       slowVelocity.div(10);
       boid.position.add(slowVelocity);
     }
   }
 }
 
 PVector getCentreOfMass(){
  PVector vec= new PVector();
  int size=0;
  for(Boid boid:boids){
    if(!boid.dead){
     vec.add(boid.position);
     size++;
    }
  }
  vec.div(size);
  return vec;
 }
 
  PVector getAverageVelocity(){
  PVector vec= new PVector();
  int size=0;
  for(Boid boid:boids){
   if(!boid.dead){
     vec.add(boid.velocity);
     size++;
   }
  }
  vec.div(size);
  return vec;
 }
 
 
 PVector flock(Boid boid){//Move toward centre of mass
   PVector com=getCentreOfMass();
   com.sub(boid.position);
   com.div(100);
   return com;
 }
 PVector avoid(Boid boid){
   PVector moveAway = new PVector();
   for(Boid boid2:boids){
     if(boid!=boid2){
       if(boid.position.dist(boid2.position)<DISTANCEBUFFER-30){
         PVector toSub=new PVector(boid2.position.x, boid2.position.y);
         toSub.sub(boid.position);
         PVector vel = new PVector(random(-10,10),random(-10,10));
         vel.sub(toSub);
         moveAway.add(vel);
       }
     }
   }
   return moveAway;
 }
 PVector matchVelocity(Boid boid){
   PVector avv=getAverageVelocity();
   avv.sub(boid.velocity);
   avv.div(8);
   return avv;
 }
 
 PVector avoidWalls(Boid boid){
   PVector moveAway = new PVector();
   
   //WALLS
   if(boid.position.x<wallleft+DISTANCEBUFFER){
     moveAway.add(new PVector(1, 0));
   }
   if(boid.position.x>wallright-DISTANCEBUFFER){
     moveAway.add(new PVector(-1, 0));
   }
   if(boid.position.y<walltop+DISTANCEBUFFER){
     moveAway.add(new PVector(0, 1));
   }
   if(boid.position.y>wallbot-DISTANCEBUFFER){
     moveAway.add(new PVector(0, -1));
   }
   //END WALLS
   return moveAway;
 }
 
 PVector avoidPoints(Boid boid){
   PVector moveAway = new PVector();
   for(PVector p : m_avoidPoints){
     if(boid.position.dist(p)<DISTANCEBUFFER){
         PVector sub=new PVector(boid.position.x, boid.position.y);
         sub.sub(p);
         moveAway.add(sub);
     }
   }
   return moveAway;
 }
 
 PVector goToPoints(Boid boid){
   PVector moveAway = new PVector();
   int size=0;
   for(PVector p : m_attractPoints){
     if(boid.position.dist(p)>DISTANCEBUFFER){
         PVector sub=new PVector(p.x,p.y);
         sub.sub(boid.position);
         moveAway.add(sub);
         size++;
     }
   }
   if(size!=0){
         buzz.amp(1*((float)size/boids.size()));
         if(!buzz.isPlaying()){
           //buzz.play(); 
         }
       }
   moveAway.div(10);
   return moveAway;
 }
 
}
