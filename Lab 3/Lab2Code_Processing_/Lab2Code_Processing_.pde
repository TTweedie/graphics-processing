PImage img;

void setup(){
 size(1280, 960);
 img = loadImage("coin.png");
}

void draw(){
 PImage greyscale=greyscale(img);
 PImage binary=greyscaleToBinary(greyscale);
 boolean[][] se={{false, true, false}, {true, true, true}, {false, true, false}}; 
 PImage dilated=dilation(binary, se, 0,1);
 image(img,0,0);
 image(greyscale, img.width, 0);
 image(binary, 0, img.height);
 image(dilated, img.width, img.height);
}

PImage greyscale(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   returnimg.pixels[i]=color(int(red(img.pixels[i])*0.212671+green(img.pixels[i])*0.715160+blue(img.pixels[i])*0.072169));  
 }
 return returnimg;
}

PImage greyscaleToBinary(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   if(color(img.pixels[i])<color(128)){
     returnimg.pixels[i]=color(0);
   }
   else{
     returnimg.pixels[i]=color(255);
   }
 }
 return returnimg;
}

PImage dilation(PImage img, boolean[][]se, int originx, int originy){
  PImage returnimg = createImage(img.width, img.height, ALPHA);
  int endloc=img.height*img.width;
  for(int imgloc=0;imgloc<endloc;imgloc++){
    boolean fillpixel=false;
    for(int i=0;i<se.length;i++){
      for(int j=0;j<se[i].length;j++){
        int offset=(j-originx)+(img.width*(i-originy));
        int serelativeloc=constrain(imgloc+offset, 0, endloc-1);
        if((color(img.pixels[serelativeloc])==color(255))&&(se[i][j])){
          fillpixel=true;
        }
    }
  }
  if(fillpixel){
    returnimg.pixels[imgloc]=color(255);
  }
  else{
   returnimg.pixels[imgloc]=color(0); 
  }
}
  return returnimg;
}
