import java.util.Random;

float BALLSIZE = 50;//Sets the size of the ball
float ZSPAWN =-200;//Sets the z spawn of the ball
float ZSPEED = -5;//Sets the z speed of the ball. - is away from the camera
float MAXXY = -5;//Sets minimum X/Y velocity for the random spawn
float MINXY = 5;//Sets minimum X/Y velocity for the random spawn
float CAMERAZ=500;//This is the z at which the camera sits
float GRAVITY=1;//This affects how strong gravity is
float FRICTION=0.2;//Between 0 and 1 - Affects how much the balls slow down when they hit the walls and floor
boolean GRAVITY_OFF_TOWARD_SCREEN=true; //Makes gravity only work when balls move away from screen as stated in the Sumbission specification

class Vector3D{
  float x,y,z;
  
  Vector3D(float x, float y, float z){
    this.x=x;
    this.y=y;
    this.z=z;
  }
  
  void add(Vector3D lvalue){
    x+=lvalue.x;
    y+=lvalue.y;
    z+=lvalue.z;
  }
}

class Ball{
  Vector3D m_position;
  Vector3D m_velocity;
  PShape m_shape;
  
  Ball(Vector3D position, Vector3D velocity, PImage texture){
    m_position=position;
    m_velocity=velocity;
    m_shape=createShape(SPHERE, BALLSIZE);
    m_shape.setTexture(texture);
  }
  
  void applyPhysics(){
    applyGravity();
    m_position.add(m_velocity);
    m_shape.resetMatrix();
    m_shape.translate(m_position.x, m_position.y,m_position.z);
  }
  
  void applyGravity(){
   if(m_velocity.z<0){
     m_velocity.y+=GRAVITY;
   }
   if(!GRAVITY_OFF_TOWARD_SCREEN){
     m_velocity.y+=GRAVITY;
   }
  }
  
  void render(){
    shape(m_shape);
  }
  
  void checkBounce(Box b){
    if(m_position.z<b.m_farZ){
      m_position.z=b.m_farZ;
      m_velocity.z*=-1+FRICTION;
    }
    if(m_position.z>b.m_cameraZ){
      m_position.z=b.m_cameraZ;
      m_velocity.z*=-1+FRICTION;
    }
    if(m_position.x<b.m_leftX){
      m_position.x=b.m_leftX;
      m_velocity.x*=-1+FRICTION;
    }
    if(m_position.x>b.m_rightX){
      m_position.x=b.m_rightX;
      m_velocity.x*=-1+FRICTION;
    }
    if(m_position.y<b.m_ceilingY){
      m_position.y=b.m_ceilingY;
      m_velocity.y*=-1+FRICTION;
    }
    if(m_position.y>b.m_floorY){
      m_position.y=b.m_floorY;
      m_velocity.y*=-1+FRICTION;
      m_velocity.x*=1-(FRICTION/6);//To slow while on floor
      m_velocity.z*=1-(FRICTION/6);//To slow while on floor
    }  
  }
}

class Box{
  float m_floorY;
  float m_ceilingY;
  float m_leftX;
  float m_rightX;
  float m_farZ;
  float m_cameraZ;
  
  Box(float ceilingY, float floorY, float leftX, float rightX,float farZ, float cameraZ){
    m_floorY=floorY;
    m_ceilingY=ceilingY;
    m_leftX=leftX;
    m_rightX=rightX;
    m_farZ=farZ;
    m_cameraZ=cameraZ;
  }
}

class Simulation{
 Box m_box;
 ArrayList<Ball> m_balls;//???
 PImage[] m_texturePool;
 
 Simulation(Box box){
   m_box=box;
   m_balls=new ArrayList<Ball>();
   m_texturePool=new PImage[7];
   m_texturePool[0]=loadImage("earth.jpg");
   m_texturePool[1]=loadImage("moon.jpg");
   m_texturePool[2]=loadImage("sun.jpg");
   m_texturePool[3]=loadImage("venus.jpg");
   m_texturePool[4]=loadImage("jupiter.jpg");
   m_texturePool[5]=loadImage("sun.jpg");
   //m_texturePool[6]=loadImage("earthnight.jpg");
   m_texturePool[6]=loadImage("mars.jpg");
 }
 
 void addBall(float x, float y){
   Random rand = new Random();
   float randX=MINXY + rand.nextFloat() * (MAXXY - MINXY);
   float randY=MINXY + rand.nextFloat() * (MAXXY - MINXY);
   Ball b = new Ball(new Vector3D(x,y,ZSPAWN),new Vector3D(randX,randY,ZSPEED), m_texturePool[rand.nextInt(7)]);
   m_balls.add(b);
 }
 
 void applyPhysics(){
   for(Ball b:m_balls){
     b.checkBounce(m_box);
     b.applyPhysics();
   }
 }
 
 void render(){
   for(Ball b:m_balls){
     b.render();
   }
 }
}

Simulation sim;

void setup(){
   size(1024,768,P3D);
   noStroke();
   sim = new Simulation(new Box(100,700,150,950,-500,CAMERAZ-500));
}
void draw(){
  background(0);
  sim.applyPhysics();
  sim.render();
}
void mouseClicked(){
  sim.addBall(mouseX, mouseY);
  println(sim.m_balls.size());
}
