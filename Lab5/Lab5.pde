import processing.video.*;

Movie toTrack;
PImage display;
boolean bol=false;

void setup(){
  size(1280, 960);
  toTrack = new Movie(this, "pingpang.mov");
  toTrack.loop();
  display=toTrack;
}

void draw(){
    if(bol){
    background(0);
    PImage toTrackDisplay=toTrack.copy();
    PImage displayDisplay=display.copy();
    toTrackDisplay.resize(200,0);
    displayDisplay.resize(200,0);
    image(toTrackDisplay, 0, 0);
    image(displayDisplay, 200, 0);
    }
}

void movieEvent(Movie m){
  m.read();
  bol=true;
  PImage frame=m;
  PImage onlyBall = greyscaleToBinary(greyscale(frame), 240);
  display=findCentreOfMass(onlyBall);
  
}

PImage greyscale(PImage img){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   returnimg.pixels[i]=color(int(red(img.pixels[i])*0.212671+green(img.pixels[i])*0.715160+blue(img.pixels[i])*0.072169));  
 }
 return returnimg;
}

PImage greyscaleToBinary(PImage img, int limit){
 PImage returnimg = createImage(img.width, img.height, ALPHA);
 int endloc=img.height*img.width;
 for (int i=0;i<endloc;i++){
   if(color(img.pixels[i])<color(limit)){
     returnimg.pixels[i]=color(0);
   }
   else{
     returnimg.pixels[i]=color(255);
   }
 }
 return returnimg;
}

PImage findCentreOfMass(PImage img){
  img.loadPixels();
  PImage returnimg = createImage(img.width, img.height, ALPHA);
  int count = 0;
  int xsum = 0;
  int ysum = 0;
  for(int y=0;y<img.height;y++){
    for(int x=0;x<img.width;x++){
      int xyloc = x+y*img.width;
      if(color(img.pixels[xyloc])==color(255)){
        count++;
        xsum+=x;
        ysum+=y;
      }
    }
  }
  int centreOfMassx=0;
  int centreOfMassy=0;
  if(count!=0){
    centreOfMassx=xsum/count;
    centreOfMassy=ysum/count;
  }
  println(count, xsum, ysum, centreOfMassx, centreOfMassy);
  
  for(int i=centreOfMassx-10;i<centreOfMassx+10;i++){
    for(int j=centreOfMassy-10;j<centreOfMassy+10;j++){
      int xyloc=constrain(j+i*width, 0, img.width*img.height);
      println(xyloc);
      returnimg.pixels[xyloc] = color(150);
    }
  }
  return returnimg;
}
