CelestialBody earth;
CelestialBody moon;
CelestialBody sun;

void setup(){
 size(1024,768,P3D);
 noStroke();
 sun= new CelestialBody(300,300,0);
 earth = new CelestialBody(100,0,0,0,0,350,0.35, "earth.jpg", sun);
 moon = new CelestialBody(50, 75,75,0,0,200,2,"moon.jpg", earth);
 
}

void draw(){
 background(0);
 translate(200,200,0);
 pointLight(255,255, 255,300,300,100);
 //lights();
 shape(earth.m_shape);
 shape(moon.m_shape);
 earth.applyPhysics();
 moon.applyPhysics();
}

class CelestialBody{
  float m_x;
  float m_y;
  float m_z;
  float m_speed;
  float m_rotation;
  PImage m_texture;
  int m_size;
  int m_orbitRadius;
  float m_orbitAngle;
  CelestialBody m_orbiting;
  PShape m_shape;
  
  CelestialBody(int x,int y,int z){
    m_x=x;
    m_y=y;
    m_z=z;
  }
  
  CelestialBody(int size, int x, int y, int z ,int rotation, int orbitRadius, float orbitSpeed, String filename, CelestialBody orbiting){
    m_x=x;
    m_y=y;
    m_z=z;
    m_rotation=rotation;
    m_size=size;
    m_orbiting=orbiting;
    m_orbitRadius=orbitRadius;
    m_orbitAngle=0;
    m_speed=orbitSpeed;
    m_texture=loadImage(filename);
    m_shape=createShape(SPHERE, size);
    m_shape.setTexture(m_texture);
    m_shape.translate(x,y,z);
  }
  void applyPhysics(){
    m_x=(m_orbiting.m_x+m_orbitRadius*cos(m_orbitAngle)); //<>//
    m_y=(m_orbiting.m_y+m_orbitRadius*sin(m_orbitAngle));
    m_rotation=m_orbitAngle;
    m_orbitAngle+=m_speed*0.01f;
    m_shape.resetMatrix();
    m_shape.rotate(m_rotation);
    m_shape.translate(m_x,m_y,m_z);
  }
}
