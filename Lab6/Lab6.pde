import processing.sound.*;
import ddf.minim.*;

LowPass fil;
Reverb fil2;
SoundFile song;

void setup()
{
 size(500,500); 
 song= new SoundFile(this, "sample-section1-2.wav");
 fil = new LowPass(this);
 fil2 = new Reverb(this);
}

void draw(){
 background(0); 
}

void mouseClicked(){
    float pos = ((float)mouseX/500+(float)mouseY/500)/2;
    println(pos);
    song.amp(pos);
  if(mouseButton == LEFT){
    fil.process(song);
  }
  if(mouseButton == RIGHT){
    fil2.process(song);
  }
    song.play();
}
