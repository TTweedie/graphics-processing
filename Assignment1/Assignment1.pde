import processing.video.*;

int width;
int height;

void setup(){
  
}

void draw(){
  
}

PImage findMotionVectors(PImage firstFrame, PImage secondFrame, int blockSize, int blockDistanceLimit, int v1, int v2){
  firstFrame.loadPixels();
  secondFrame.loadPixels();
  for(int i =0;i<firstFrame.width/blockSize; i++){
    for(int j=0;j<firstFrame.height/blockSize;j+=blockSize){

    }
  }
  return firstFrame;
}

float SumSquaredDistance(int[] block1, int[] block2, int blockSize){
  int tosqrt=0;
  for(int i=0; i<blockSize;i++){
    for(int j=0; j<blockSize;j++){
      tosqrt+=(block1[i+j*blockSize]-block2[i+j*blockSize]);
    }
  }
  return sqrt(tosqrt);
}

float[] getSSDArray(PImage firstFrame, PImage secondFrame, int blockSize, int blockDistanceLimit, int blockX, int blockY){
  float[] ssdArray= new float[(blockDistanceLimit*2+1)^2];
  int[] block1=new int[blockSize*blockSize];
  for(int i=0;i<blockSize*blockSize;i++){
    block1[i]=firstFrame.pixels[blockX+blockY*firstFrame.height+i];
  }
  for(int i=0;i<blockDistanceLimit*3;i++){
    for(int j=0;j<blockDistanceLimit*3;i++){
        int[] block1=new int[blockSize*blockSize];
      for(int i=0;i<blockSize*blockSize;i++){
    block1[i]=firstFrame.pixels[blockX+blockY*firstFrame.height+i];
  }
    }
  }
  
  return ssdArray;
}
